﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using SecureFlight.Api.Models;
using SecureFlight.Api.Utils;
using SecureFlight.Core.Entities;
using SecureFlight.Core.Interfaces;

namespace SecureFlight.Api.Controllers;

[ApiController]
[Route("[controller]")]
public class FlightsController(IMapper mapper)
    : SecureFlightBaseController(mapper)
{
    [HttpGet]
    [ProducesResponseType(typeof(IEnumerable<FlightDataTransferObject>), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorResponseActionResult))]
    public async Task<IActionResult> Get([FromServices] IService<Flight> flightService)
    {
        var flights = await flightService.GetAllAsync();
        return MapResultToDataTransferObject<IReadOnlyList<Flight>, IReadOnlyList<FlightDataTransferObject>>(flights);
    }

    [HttpPost("flight/{flightId}/passenger/{passengerId}")]
    public async Task<IActionResult> AddPassenger(
        [FromServices] IService<Flight> flightService,
        [FromServices] IRepository<Flight> flightRepository,
        [FromServices] IService<Passenger> passengerService,
        [FromRoute] long flightId,
        [FromRoute] string passengerId,
        CancellationToken cancellationToken)
    {
        Flight? flight = await flightService.FindAsync(flightId);
        if (flight is null)
        {
            return NotFound($"Flight {flightId} was not found");
        }

        Passenger? passenger = await passengerService.FindAsync(passengerId);
        if (passenger is null)
        {
            return NotFound($"Passenger {passengerId} was not found");
        }

        flight.Passengers.Add(passenger);
        flightRepository.Update(flight);
        await flightRepository.SaveChangesAsync(cancellationToken);

        return Ok();
    }

    [HttpDelete("flight/{flightId}/passenger{passengerId}")]
    public async Task<IActionResult> RemovePassenger(
        [FromServices] IService<Flight> flightService,
        [FromServices] IRepository<Flight> flightRepository,
        [FromServices] IService<Passenger> passengerService,
        [FromServices] IRepository<Passenger> passengerRepository,
        [FromRoute] long flightId,
        [FromRoute] string passengerId,
        CancellationToken cancellationToken)
    {
        Flight? flight = await flightService.FindAsync(flightId);
        if (flight is null)
        {
            return NotFound($"Flight {flightId} was not found");
        }

        Passenger? passenger = await passengerService.FindAsync(passengerId);
        if (passenger is null)
        {
            return NotFound($"Passenger {passengerId} was not found");
        }

        flight.Passengers.Remove(passenger);
        flightRepository.Update(flight);

        if (!passenger.PassengerFlights.Any())
        {
            passengerRepository.Remove(passenger);
        }

        await passengerRepository.SaveChangesAsync(CancellationToken.None);

        return Ok();
    }
}